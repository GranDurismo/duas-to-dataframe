# DUA to dataframe

This R script turns Uruguayan Customs Office (DNA) export/import data into dataframes.

The raw data is organized in monthly XML files containing around 140-160k DUAs (Unique Customs Statement), which refer to a single export or import transaction. Each of these has information for 191 variables, meaning that each monthly XML reaches around 1 GB.

My first goal was to turn these XMLs into usable dataframes for further processing within R or exporting to other programs. At first I tried with the xml2 package but found that the old XML package took significantly less time (around 20-30 minutes on my 2015 13" MBP, compared to several hours with xml2).

Then I tried to prune the data a bit, dropping unnecessary variables (ended up with 62), converting classes, etc.

Disclaimers:
1) This is a WIP
2) This is my first time working with R or XMLs.